import './Footer.css'
import logoTOK from "../../images/capacete.png"
import cityRok from "../../images/city1.webp"


function Footer(props){

    return(
        <footer clossName="rodape">
            <div>
                <h2>CONQUER THE WORLD AND BECOME
                    THE AUTHOR OF YOUR KINGDOM'S HISTORY</h2>
            </div>
           <img src={logoTOK} alt="logoTOK"></img>
           <img src={cityRok} alt="cityRok"></img>
        </footer>
    )

}

export default Footer

