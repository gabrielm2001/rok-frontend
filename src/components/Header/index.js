import './Header.css'
import rokLogo from "../../images/logoRok.png"
// import lilifLogo from "../../images/lilit.png"


function Header(props){

    return(
        <header clossName="header">
            <div>
                <img src={rokLogo} alt="rok_icon"></img>
                <h1>Guia de comandantes</h1>
            </div>

            <nav>
                <ul>
                    <li>Comandantes</li>
                    <li>Login</li>
                    <li>Cadastro</li>
                </ul>
            </nav>
        </header>
    )

}

export default Header

